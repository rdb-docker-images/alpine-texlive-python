FROM python:alpine3.10

MAINTAINER "Ruben Di Battista" <rubendibattista@gmail.com>

RUN  apk update && \
     apk add texlive-full biber ghostscript
    
